from django.urls import path
from django.urls import include
# from rest_framework.routers import SimpleRouter
from rest_framework_nested import routers
from . import views


router = routers.DefaultRouter()
router.register('products', views.ProductViewSet, basename='products')
router.register('collections', views.CollectionViewSet)
router.register('carts', views.CartViewSet)
# second para 'products': prefix, lookup='product' means we are going to have a parameter 
# called product_pk in our route
products_router = routers.NestedDefaultRouter(router, 'products', lookup='product') # lookup='product_pk'
products_router.register('reviews', views.ReviewViewSet, basename='product-reviews') # regester child resource

carts_router = routers.NestedDefaultRouter(router, 'carts', lookup='cart')  # loopup='cart_pk' which is extracted in CartItemViewSet
carts_router.register('items', views.CartItemViewSet, basename='cart-items') # We hava two routes: cart-items-list, cart-item-detail


# URLConf
urlpatterns = router.urls + products_router.urls + carts_router.urls


# urlpatterns = [
#     path('', include(router.urls)),        
#     path('products/', views.ProdictList.as_view()),
#     path('products/<int:pk>/', views.ProductDetail.as_view()),
#     path('collections/', views.CollectionList.as_view()),
#     path('collections/<int:pk>/', views.CollectionDetail.as_view(), name='collection-detail'),
# ]
